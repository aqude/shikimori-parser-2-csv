package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/gocolly/colly"
)

type Anime struct {
	TitleEN     string
	TitleRU     string
	URL         string
	Year        string
	Type        string
	ImageURL    string
	Discription string
}

func main() {
	maxPages := 10

	var allAnime []Anime
	// Парсим данные с каждой страницы
	for page := 1; page <= maxPages; page++ {
		url := fmt.Sprintf("https://shikimori.me/animes/page/%d", page)

		articleCounter, animeList := parse(url)
		fmt.Printf("Количество аниме на странице %d: %d\n", page, articleCounter)

		// Добавляем результаты парсинга текущей страницы в общий срез
		allAnime = append(allAnime, animeList...)
		time.Sleep(5 * time.Second)
	}

	// Сохраняем все данные в CSV файл
	if err, i := saveToCSV(allAnime, "anime_data.csv"); err != nil {
		fmt.Println("Ошибка при сохранении в CSV файл:", err)
	} else {
		fmt.Println("Файл успешно создан!")
		fmt.Println("Успешно спаршено", i)
	}
}

func parse(url string) (int, []Anime) {
	c := colly.NewCollector()
	c.Limit(&colly.LimitRule{
		Delay: 2 * time.Second,
	})
	// Число
	articleCounter := 0

	var animeList []Anime

	//section.l-content b-search-results

	c.OnHTML("div.cc-entries", func(entries *colly.HTMLElement) {
		// Inner OnHTML callback to target each article inside cc-entries
		entries.ForEach("article.c-column", func(i int, e *colly.HTMLElement) {

			articleCounter++
			anime := Anime{
				TitleEN:  e.ChildText("span.name-en"),
				TitleRU:  e.ChildText("span.name-ru"),
				URL:      e.ChildAttr("a.title", "href"),
				Year:     e.ChildText("span.misc > span.right"),
				Type:     e.ChildText("span.misc > span:last-child"),
				ImageURL: e.ChildAttr("img", "src"),
			}
			fmt.Println(anime.TitleRU, articleCounter)
			descCollector := colly.NewCollector()
			descCollector.Limit(&colly.LimitRule{
				Delay: 2 * time.Second,
			})
			descCollector.OnHTML("div.b-text_with_paragraphs", func(e *colly.HTMLElement) {
				anime.Discription = strings.TrimSpace(e.Text)
				animeList = append(animeList, anime)
			})
			descCollector.Visit(anime.URL)
		})
	})

	c.Visit(url)

	return articleCounter, animeList
}

func saveToCSV(animeList []Anime, filename string) (error, int) {
	var i int
	file, err := os.Create(filename)
	if err != nil {
		return err, i
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	headers := []string{"Название аниме (EN)", "Название аниме (RU)", "URL аниме", "Год выпуска", "Тип аниме", "Ссылка на изображение", "Описание аниме", "size"}
	writer.Write(headers)

	for _, anime := range animeList {
		i++
		row := []string{
			anime.TitleEN,
			anime.TitleRU,
			anime.URL,
			anime.Year,
			anime.Type,
			anime.ImageURL,
			anime.Discription,
			fmt.Sprint(i),
		}
		writer.Write(row)
	}
	return nil, i
}
